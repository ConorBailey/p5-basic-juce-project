//
//  ButtonRow.cpp
//  JuceBasicWindow
//
//  Created by Conor Bailey on 25/10/2016.
//
//

#include "ButtonRow.hpp"

ButtonRow::ButtonRow()
{
    for (int i = 0; i < 5; i++)
    {
        addAndMakeVisible(buttons[i]);
    }
//    
//    addAndMakeVisible(button0);
//    addAndMakeVisible(button1);
//    addAndMakeVisible(button2);
//    addAndMakeVisible(button3);
//    addAndMakeVisible(button4);

    
}
ButtonRow::~ButtonRow()
{
    
}

void ButtonRow::resized()
{
    for (int i = 0; i < 5; i++)
    {
        buttons[i].setBounds(i * 100, 10, 90, getHeight());
    }
    
//    button0.setBounds(0, 0, 90, getHeight());
//    button1.setBounds(100, 10, 90, getHeight());
//    button2.setBounds(200, 10, 90, getHeight());
//    button3.setBounds(300, 10, 90, getHeight());
//    button4.setBounds(400, 10, 90, getHeight());

}