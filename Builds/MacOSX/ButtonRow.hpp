//
//  ButtonRow.hpp
//  JuceBasicWindow
//
//  Created by Conor Bailey on 25/10/2016.
//
//

#ifndef ButtonRow_hpp
#define ButtonRow_hpp

#include <stdio.h>
#include "../JuceLibraryCode/JuceHeader.h"
#include "newButton.hpp"


class ButtonRow : public Component
{
public:
    ButtonRow();
    ~ButtonRow();
    
    void resized() override;
    
private:
    newButton buttons[5];
    
//    newButton button0;
//    newButton button1;
//    newButton button2;
//    newButton button3;
//    newButton button4;

    
};

#endif /* ButtonRow_hpp */
