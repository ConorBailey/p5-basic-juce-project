//
//  newButton.hpp
//  JuceBasicWindow
//
//  Created by Conor Bailey on 24/10/2016.
//
//

#ifndef newButton_hpp
#define newButton_hpp

#include "../JuceLibraryCode/JuceHeader.h"


class newButton  : public Component
{
public:
    //==============================================================================
    newButton();
    ~newButton();
    
    void resized() override;
        
    void paint (Graphics& g) override;
    

    
private:

    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (newButton)
};



#endif /* newButton_hpp */
